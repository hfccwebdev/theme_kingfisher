jQuery(document).ready(function ($) {
  if ($(".kingfisher-form").length) {
    $(
      "input:not(input[type=search],input[type=checkbox],input[type=radio],input[type=submit]), textarea, select"
    ).each(function (i) {
      var inputText = $(this).val();
      var placeholder = $(this).attr("placeholder");
      var siblingLabel = $(this).siblings("label");

      if (!(inputText == "" || inputText == null)) {
        $(siblingLabel).addClass("hfc-float-label");
      }

      if (!(placeholder == "" || placeholder == null)) {
        $(siblingLabel).addClass("hfc-float-label");
      }
    });

    var positionLabel = $(
      "input:not(input[type=search],input[type=checkbox],input[type=radio],input[type=submit]), textarea, select"
    ).siblings("label");
    $(positionLabel).addClass("hfc-float-position");

    var placeholder = $(
      "input:not(input[type=search],input[type=checkbox],input[type=radio],input[type=submit]), textarea, select"
    ).attr("placeholder");

    if (
      $(
        "input:not(input[type=search],input[type=checkbox],input[type=radio],input[type=submit]), textarea, select"
      ).attr("placeholder")
    ) {
      var siblingLabel = $(this).siblings("label");
      $(siblingLabel).addClass("hfc-float-label");
    }

    $(
      "input:not(input[type=search],input[type=checkbox],input[type=radio],input[type=submit]), textarea, select"
    ).focus(function () {
      var siblingLabel = $(this).siblings("label");
      $(siblingLabel).addClass("hfc-float-label");
    });

    $(
      "input:not(input[type=search],input[type=checkbox],input[type=radio],input[type=submit]), textarea, select"
    ).blur(function () {
      var inputText = $(this).val();
      var siblingLabel = $(this).siblings("label");

      if (
        inputText === "" ||
        inputText === null ||
        ($(this).attr("placeholder") === "" || $(this).attr("placeholder")) ===
          ""
      ) {
        $(siblingLabel).removeClass("hfc-float-label");
      } else {
        $(siblingLabel).addClass("hfc-float-label");
      }
    });
  }
});
