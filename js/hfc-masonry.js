jQuery(window).on("load", function () {
  var $ = jQuery;

  var $grid = $(".masonry-view").imagesLoaded(function () {
    $grid.masonry({
      // options
      itemSelector: ".views-row",
      transitionDuration: "0.1s",
      gutter: 8,
      fitWidth: true,
      resize: true,
    });
  });
});
